#!/bin/bash

git clone https://github.com/wodby/docker4drupal/ docker4drupal
cd docker4drupal
# Get new tags from remote
git fetch --tags
# Get latest tag name
latestTag=$(git describe --tags `git rev-list --tags --max-count=1`)
# Checkout latest tag
git checkout $latestTag
cd ..


# if [ ! -f .env ];
# then
  cp docker4drupal/.env .env
  sed -i "s/my_drupal9_project/${PWD##*/}/" .env
  sed -i "s/drupal.docker/${PWD##*/}/" .env
  echo "" >> .env
  echo "" >> .env
  echo "DRUSH_OPTIONS_URI=${PWD##*/}.localhost" >> .env
# fi

# if [ ! -f docker-compose.yml ];
# then
  cp docker4drupal/docker-compose.yml docker-compose.yml
  echo "  node:" >> docker-compose.yml
  echo "    image: wodby/node:\$NODE_TAG" >> docker-compose.yml

# fi

# if [ ! -f docker-compose.override.yml ];
# then
  cp drupalwizard/docker-compose.override.yml docker-compose.override.yml
# fi

# generate certs
# if [ ! -d private-files/certs ]; then
  mkdir -p private-files/certs
  openssl req -x509 -nodes -days 999999 -newkey rsa:2048 -keyout private-files/certs/cert.key -out private-files/certs/cert.crt -subj "/C=ES/ST=Madrid/L=Madrid/O=carcheky/OU=carcheky/CN=${projectname}.localhost/emailAddress=user@localhost"
# fi

rm -fr docker4drupal
docker compose up -d
